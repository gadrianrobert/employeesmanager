﻿using System;
using System.Collections.Generic;
using System.Web;
using Newtonsoft.Json;
using SMCI.Core.Utils;
using Action = SMCI.Core.Utils.Action;
using System.Linq;
using SMCI.Core.DataLayer;

namespace SMCI.Core.Handlers
{
    /// <summary>
    /// Class that handles user actions.
    /// </summary>
    public class PerformAction : IHttpHandler
    {
        #region Private Memmbers

        private const string ActionKey = "action";
        private const string DataKey = "data";
        private const string NameFilterKey = "namefilter";
        private const string DepartmentFilterKey = "departmentfilter";

        private const string ErrorMessage = "Error occured!";

        private static HR HR = new HR();

        private static Dictionary<int, string> Departments = HR.Departments.ToDictionary(Dept => Dept.DepartmentId, Dept => Dept.Name);

        #endregion

        #region Public Methods

        /// <summary>
        /// Enables processing of HTTP Web requests by a custom HttpHandler that implements the <see cref="T:System.Web.IHttpHandler"/> interface.
        /// </summary>
        /// <param name="context">An <see cref="T:System.Web.HttpContext"/> object that provides references to the intrinsic server objects (for example, Request, Response, Session, and Server) used to service HTTP requests. </param>
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            var action = context.Request.Form[ActionKey];

            switch (action)
            {
                case Action.GetEmployees:
                    context.Response.Write(JsonConvert.SerializeObject(GetEmployees(context)));
                    break;
                case Action.GetEmployee:
                    context.Response.Write(JsonConvert.SerializeObject(GetEmployee(context)));
                    break;
                case Action.DeleteEmployee:
                    context.Response.Write(JsonConvert.SerializeObject(DeleteEmployee(context)));
                    break;
                case Action.CreateEmployee:
                    context.Response.Write(JsonConvert.SerializeObject(CreateEmployee(context)));
                    break;
                case Action.EditEmployee:
                    context.Response.Write(JsonConvert.SerializeObject(EditEmployee(context)));
                    break;
                case Action.FilterEmployees:
                    var name = context.Request.Form[NameFilterKey];
                    var department = context.Request.Form[DepartmentFilterKey];
                    context.Response.Write(JsonConvert.SerializeObject(GetEmployees(context, name, department)));
                    break;
                default:
                    SetErrorStatus(context, ErrorMessage);
                    break;
            }
        }

        /// <summary>
        /// Gets a value indicating whether another request can use the <see cref="T:System.Web.IHttpHandler"/> instance.
        /// </summary>
        /// 
        /// <returns>
        /// true if the <see cref="T:System.Web.IHttpHandler"/> instance is reusable; otherwise, false.
        /// </returns>
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Retrieves the employees collection.
        /// </summary>
        /// <param name="context">The http context instance.</param>
        /// <param name="name">The employee name.</param>
        /// <param name="department">The employee department.</param>
        /// <returns>The list of employees.</returns>
        private List<BasicEmployee> GetEmployees(HttpContext context, string name = null, string department = null)
        {
            List<BasicEmployee> result = new List<BasicEmployee>();
            try
            {
                var employees = HR.Employees.ToList();
                foreach (var employee in employees)
                {
                    var departmentName = Departments[employee.DepartmentId];
                    if (employee.EmployeeName.ToLower().Contains(name != null ? name.ToLower() : string.Empty) &&
                        departmentName.ToLower().Contains(department != null ? department.ToLower() : string.Empty))
                    {
                        result.Add(new BasicEmployee() { Id = employee.EmployeeId, Name = employee.EmployeeName, DepartmentName = departmentName });
                    }
                }
            }
            catch (Exception ex)
            {
                Global.Logger.Error(ErrorMessage, ex);
                SetErrorStatus(context, ErrorMessage);
            }
            return result;
        }

        /// <summary>
        /// Retrieves an employee based on the id that is posted with the request.
        /// </summary>
        /// <param name="context">The http context instance.</param>
        /// <returns>The employee instance.</returns>
        private BasicEmployee GetEmployee(HttpContext context)
        {
            var data = context.Request.Form[DataKey];
            var result = new BasicEmployee() { DepartmentName = Departments.First().Value };
            try
            {
                var basicEmployee = JsonConvert.DeserializeObject<BasicEmployee>(data);
                var employee = HR.Employees.SingleOrDefault(Employee => Employee.EmployeeId == basicEmployee.Id);
                if (employee != null)
                {
                    result = new BasicEmployee() { Id = employee.EmployeeId, Name = employee.EmployeeName, DepartmentName = Departments[employee.DepartmentId] };
                }
            }
            catch (Exception ex)
            {
                Global.Logger.Error(ErrorMessage, ex);
                SetErrorStatus(context, ErrorMessage);
            }
            return result;
        }

        /// <summary>
        /// Deletes an employee based on the id that is posted with the request.
        /// </summary>
        /// <param name="context">The http context instance.</param>
        /// <returns>The list of employees without the deleted one.</returns>
        private List<BasicEmployee> DeleteEmployee(HttpContext context)
        {
            var data = context.Request.Form[DataKey];
            try
            {
                var employee = JsonConvert.DeserializeObject<BasicEmployee>(data);
                HR.Employees.Remove(HR.Employees.Single(Employee => Employee.EmployeeId == employee.Id));
                HR.SaveChanges();
            }
            catch (Exception ex)
            {
                Global.Logger.Error(ErrorMessage, ex);
                SetErrorStatus(context, ErrorMessage);
            }
            return GetEmployees(context);
        }

        /// <summary>
        /// Creates an employee based on the data that is posted with the request.
        /// </summary>
        /// <param name="context">The http context instance.</param>
        /// <returns>The list of employees including the new inserted record.</returns>
        private List<BasicEmployee> CreateEmployee(HttpContext context)
        {
            var data = context.Request.Form[DataKey];
            try
            {
                var employee = JsonConvert.DeserializeObject<BasicEmployee>(data);
                HR.Employees.Add(new Employee() { EmployeeId = employee.Id, EmployeeName = employee.Name, DepartmentId = Departments.First(Department => Department.Value == employee.DepartmentName).Key });
                HR.SaveChanges();
            }
            catch (Exception ex)
            {
                Global.Logger.Error(ErrorMessage, ex);
                SetErrorStatus(context, ErrorMessage);
            }
            return GetEmployees(context);
        }

        /// <summary>
        /// Edit an employee based on the data that is posted with the request.
        /// </summary>
        /// <param name="context">The http context instance.</param>
        /// <returns>The updated employee record.</returns>
        private BasicEmployee EditEmployee(HttpContext context)
        {
            var data = context.Request.Form[DataKey];
            var result = new BasicEmployee();
            try
            {
                var basicEmployee = JsonConvert.DeserializeObject<BasicEmployee>(data);
                var employee = HR.Employees.First(Employee => Employee.EmployeeId == basicEmployee.Id);
                employee.EmployeeName = basicEmployee.Name;
                employee.DepartmentId = Departments.First(Department => Department.Value == basicEmployee.DepartmentName).Key;
                result = basicEmployee;
                HR.SaveChanges();
            }
            catch (Exception ex)
            {
                Global.Logger.Error(ErrorMessage, ex);
                SetErrorStatus(context, ErrorMessage);
            }
            return result;
        }

        /// <summary>
        /// Sets response internal error status code and outputs the error message.
        /// </summary>
        /// <param name="context">The http context instance.</param>
        /// <param name="message">The error message.</param>
        private void SetErrorStatus(HttpContext context, string message)
        {
            context.Response.StatusCode = 500;
            context.Response.Write(message);
        }

        #endregion

    }
}