﻿using System;

namespace SMCI.Core.Utils
{
    /// <summary>
    /// Basic employee entity.
    /// </summary>
    public class BasicEmployee
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String DepartmentName { get; set; }
    }
}