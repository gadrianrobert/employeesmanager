﻿namespace SMCI.Core.Utils
{
    /// <summary>
    /// Class that holds user action constants.
    /// </summary>
    public static class Action
    {
        public const string GetEmployees = "getemployees";
        public const string GetEmployee = "getemployee";
        public const string DeleteEmployee = "deleteemployee";
        public const string CreateEmployee = "createemployee";
        public const string EditEmployee = "editemployee";
        public const string FilterEmployees = "filteremployees";
    }
}