﻿<%@ Page Title="Add/Edit Employee" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Edit.aspx.cs" Inherits="SMCI.About" %>
<%@ MasterType  virtualPath="~/Site.master"%>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <% if (Master.IsAuthenticated)
       { %>

    <div id="employeeContainer">
    </div>
    <script id="tmplCreateEditEmployee" type="text/x-jsrender">
        <div>
            {^{if employee}}
                <input id="hdnId" type="hidden" data-link="employee.Id" />
            {{/if}}
            <table>
                <tbody>
                    <tr>
                        <td><label for="txtName">Name</label></td>
                        <td><input id="txtName" type="text" data-link="employee.Name" /></td>
                    </tr>
                    <tr>
                        <td><label for="selDepartment">Department</label></td>
                        <td><select id="selDepartment" data-link="employee.DepartmentName">
                                {^{for departments}}
                                    {^{if ~root.employee && ~root.employee.DepartmentName==name}}
                                        <option value="{{:name}}" selected="true">{{:name}}</option>
                                    {{else}}
                                        <option value="{{:name}}">{{:name}}</option>
                                    {{/if}}
                                {{/for}}
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            {^{if !(employee && employee.Id)}}
                <br>
                <input id="btnCreateEmployee" type="button" onclick="Service.CreateEmployee($('#txtName').val(), $('#selDepartment').val(), true);return false;" value="Create Employee"/>
            {{else}}
                <br>
                <input id="btnEditEmployee" type="button" onclick="Service.EditEmployee();return false;" value="Save Changes"/>
                <input id="btnDeleteEmployee" type="button" onclick="Service.DeleteEmployee($('#hdnId').val(), true);return false;" value="Delete Employee"/>
            {{/if}}
        </div>
    </script>
    <script type="text/javascript">
        Service.RenderEmployee(<%=employeeId%>);
    </script>
    <% } %>
</asp:Content>
