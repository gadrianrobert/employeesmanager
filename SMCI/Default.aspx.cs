﻿using System;
using System.Web.Security;

namespace SMCI
{
    public partial class _Default : System.Web.UI.Page
    {
        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}
