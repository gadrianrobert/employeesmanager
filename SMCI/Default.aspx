﻿<%@ Page Title="View Employees" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="SMCI._Default" %>
<%@ MasterType  virtualPath="~/Site.master"%>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <% if (Master.IsAuthenticated)
       { %>

    <div id="employeesContainer">
    </div>

    <script id="tmplViewEmployees" type="text/x-jsrender">
        {{if (employees && employees.length) || filtering}}
            <table id="tbFilter">
                <tr>
                    <td><label for="txtNameFilter">Name</label></td>
                    <td><input id="txtNameFilter" type="text" value="{{:nameFilter}}" /></td>
                </tr>
                <tr>
                    <td><label for="selDepartmentFilter">Department</label></td>
                    <td>
                        <select id="selDepartmentFilter" >
                            <option value="">Select Department</option>
                            {{for departments}}
                                {{if ~root.departmentFilter==name}}
                                    <option value="{{:name}}" selected="true">{{:name}}</option>
                                {{else}}
                                    <option value="{{:name}}">{{:name}}</option>
                                {{/if}}
                            {{/for}}
                        </select>
                    </td>
                </tr>
            </table>
            <br>
            <input id="btnFilterEmployees" type="button" onclick="Service.FilterEmployees();return false;" value="Filter Employees"/>
            <input id="btnCancelFilterEmployees" type="button" onclick="Service.RenderEmployees();return false;" value="Cancel Filter"/>
            <br><br>  
            {{if employees && employees.length}}
                <table id="tbEmployees" border="1">
                    <tbody>
                        <tr>   
                            {{!--<th></th> 
                            <th>Id</th>--}}
                            <th>Name</th>
                            <th>Department Name</th>
                            <th></th>
                            <th></th>
                        </tr>
                        {{for employees}}
                            <tr>    
                                {{!--<td><input id="rd{{:Id}}" name="rdSelected" type="radio" value="{{:Id}}"/></td>
                                <td>{{:Id}}</td>--}}
                                <td>{{:Name}}</td>
                                <td>{{:DepartmentName}}</td>
                                <td><img class="remove" src="/styles/edit.png" onclick="Service.GoToEditEmployeePage({{:Id}});return false;" /></td>
                                <td><img class="remove" src="/styles/close.png" onclick="Service.DeleteEmployee({{:Id}});return false;" /></td>
                            </tr>
                        {{/for}}
                    </tbody>
                </table>
            {{else}}
                No employees were found!
                <br><br>
            {{/if}}
            <br>
            <input id="btnCreateEmployee" type="button" onclick="Service.ShowCreateEmployeeDialog();return false;" value="Add Employee"/>
            {{!--<input id="btnEditEmployee" type="button" onclick="Service.ShowEditEmployeeDialog();return false;" value="Edit Employee"/>--}}
        {{else}}
            No employees were found!
            <br><br>
            <input id="btnCreateEmployee" type="button" onclick="Service.ShowCreateEmployeeDialog();return false;" value="Create Employee"/>
        {{/if}}
    </script>

    <script id="tmplCreateEditEmployee" type="text/x-jsrender">
        <div>
            <input id="hdnId" type="hidden" value="{{if employee}}{{:employee.Id}}{{/if}}" />
            <table>
                <tbody>
                    <tr>
                        <td><label for="txtName">Name</label></td>
                        <td><input id="txtName" type="text" value="{{if employee}}{{:employee.Name}}{{/if}}" /></td>
                    </tr>
                    <tr>
                        <td><label for="selDepartment">Department</label></td>
                        <td><select id="selDepartment">
                                {{for departments}}
                                    {{if ~root.employee && ~root.employee.DepartmentName==name}}
                                        <option value="{{:name}}" selected="true">{{:name}}</option>
                                    {{else}}
                                        <option value="{{:name}}">{{:name}}</option>
                                    {{/if}}
                                {{/for}}
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </script>
    <script type="text/javascript">
        Service.RenderEmployees();
    </script>
    <% } %>
</asp:Content>
