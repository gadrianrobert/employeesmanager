﻿//********************************************************
//Object that holds user actions
//********************************************************
var Action = {
    GetEmployees: "getemployees",
    GetEmployee: "getemployee",
    DeleteEmployee: "deleteemployee",
    CreateEmployee: "createemployee",
    EditEmployee: "editemployee",
    FilterEmployees: "filteremployees"
};

//********************************************************
//Object that holds ajax call types
//********************************************************
var AjaxCallType = {
    Get: "GET",
    Post: "POST",
    Put: "PUT"
};

//********************************************************
//Object that holds block UI options
//********************************************************
var BlockUIOptions = {
    css: {
        border: 'none',
        padding: '15px',
        backgroundColor: '#000',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .5,
        color: '#fff'
    }
};

//********************************************************
//Service object that handles client server requests
//********************************************************
var Service = {
    Employees: {},
    CurrentEmployee: {},
    //********************************************************
    //Method for performing ajax requests
    //********************************************************
    PerformAction: function (typeInfo, asyncInfo, actionInfo, dataInfo, nameFilterInfo, departmentFilterInfo, successHandler, errorHandler) {
        var options = {
            url: "/core/handlers/performaction.ashx?timestamp=" + new Date().getTime(),
            type: typeInfo,
            async: asyncInfo,
            cache: false,
            data: { action: actionInfo, data: dataInfo ? JSON.stringify(dataInfo) : null, namefilter: nameFilterInfo, departmentfilter: departmentFilterInfo },
            success: function (response) {
                if (successHandler) {
                    successHandler(response);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                if (errorHandler) {
                    errorHandler(xhr, ajaxOptions, thrownError);
                }
            }
        };

        return $.ajax(options);
    },
    //********************************************************
    //Method for execute render employees template
    //********************************************************
    ExecuteRenderEmployeesTemplate: function (employeesInfo, nameFiterInfo, departmentFilterInfo) {
        var app = {
            employees: employeesInfo,
            nameFilter: nameFiterInfo,
            departmentFilter: departmentFilterInfo,
            departments: Departments,
            filtering: nameFiterInfo || departmentFilterInfo
        };
        var myTemplate = $.templates("#tmplViewEmployees");
        var html = myTemplate.render(app);
        $("#employeesContainer").html(html);
    },
    //********************************************************
    //Method for execute render employee template
    //********************************************************
    ExecuteRenderEmployeeTemplate: function (employeeInfo) {
        var app = {
            employee: employeeInfo ? employeeInfo : this.CurrentEmployee,
            departments: Departments
        };
        var myTemplate = $.templates("#tmplCreateEditEmployee");
        myTemplate.link("#employeeContainer", app);
    },
    //********************************************************
    //Method for render employees on the UI
    //********************************************************
    RenderEmployees: function () {
        var self = this;
        this.PerformAction(AjaxCallType.Post, true, Action.GetEmployees, null, null, null,
            function (response) {
                self.Employees = response;
                self.ExecuteRenderEmployeesTemplate(response);
            },
            function (xhr, ajaxOptions, thrownError) {
                self.ShowMessageDialog("Error", xhr.responseText);
            });
    },
    //********************************************************
    //Method for  render an employee on the UI
    //********************************************************
    RenderEmployee: function (idInfo) {
        var self = this;
        this.PerformAction(AjaxCallType.Post, true, Action.GetEmployee, { Id: idInfo }, null, null,
            function (response) {
                self.CurrentEmployee = response;
                self.ExecuteRenderEmployeeTemplate(response);
            },
            function (xhr, ajaxOptions, thrownError) {
                self.ShowMessageDialog("Error", xhr.responseText);
            });
    },
    //********************************************************
    //Method for filtering and rendering the employees on the UI
    //********************************************************
    FilterEmployees: function () {
        var self = this;
        var nameFilter = $('#txtNameFilter').val();
        var departmentFilter = $('#selDepartmentFilter').val();
        this.PerformAction(AjaxCallType.Post, true, Action.FilterEmployees, null, nameFilter, departmentFilter,
            function (response) {
                self.Employees = response;
                self.ExecuteRenderEmployeesTemplate(response, nameFilter, departmentFilter);
            },
            function (xhr, ajaxOptions, thrownError) {
                self.ShowMessageDialog("Error", xhr.responseText);
            });
    },
    //********************************************************
    //Method for deleting an employee
    //********************************************************
    DeleteEmployee: function (idInfo, viewAllEmployees) {
        var self = this;
        this.PerformAction(AjaxCallType.Post, true, Action.DeleteEmployee, { Id: idInfo }, null, null,
            function (response) {
                if (viewAllEmployees) {
                    document.location = "/";
                }
                self.Employees = response;
                self.ExecuteRenderEmployeesTemplate(response);
            },
            function (xhr, ajaxOptions, thrownError) {
                self.ShowMessageDialog("Error", xhr.responseText);
            });
    },
    //********************************************************
    //Method for creating an employee
    //********************************************************
    CreateEmployee: function (nameInfo, departmentInfo, viewAllEmployees) {
        var self = this;
        this.PerformAction(AjaxCallType.Post, true, Action.CreateEmployee, { Name: nameInfo, DepartmentName: departmentInfo }, null, null,
            function (response) {
                if (viewAllEmployees) {
                    document.location = "/";
                }
                self.Employees = response;
                self.ExecuteRenderEmployeesTemplate(response);
            },
            function (xhr, ajaxOptions, thrownError) {
                self.ShowMessageDialog("Error", xhr.responseText);
            });
    },
    //********************************************************
    //Method for editing an employee
    //********************************************************
    EditEmployee: function () {
        var self = this;
        this.PerformAction(AjaxCallType.Post, true, Action.EditEmployee, self.CurrentEmployee, null, null,
            function (response) {
            },
            function (xhr, ajaxOptions, thrownError) {
                self.ShowMessageDialog("Error", xhr.responseText);
            });
    },
    //********************************************************
    //Method for directing the user to the edit page
    //********************************************************
    GoToEditEmployeePage: function (idInfo) {
        document.location = "/Edit.aspx?id=" + idInfo;
    },
    //********************************************************
    //Method for showing message dialogs
    //********************************************************
    ShowMessageDialog: function (titleInfo, messageInfo) {
        var self = this;
        $("<span>" + messageInfo + "</span>").dialog({
            title: titleInfo,
            buttons: [
                {
                    text: "Ok",
                    click: function () {
                        $(this).dialog("destroy").remove();
                    }
                }]
        });
    },
    //********************************************************
    //Method for showing create employee dialog
    //********************************************************
    ShowCreateEmployeeDialog: function () {
        var self = this;
        var app = { departments: Departments };
        var myTemplate = $.templates("#tmplCreateEditEmployee");
        var html = myTemplate.render(app);
        $(html).dialog({
            title: "Add New Employee",
            buttons: [
                {
                    text: "Add",
                    click: function () {
                        self.CreateEmployee($('#txtName').val(), $('#selDepartment').val());
                        $(this).dialog("destroy").remove();
                    }
                },
                {
                    text: "Cancel",
                    click: function () {
                        $(this).dialog("destroy").remove();
                    }
                }]
        });
    },
    //********************************************************
    //Method for showing edit employee dialog
    //********************************************************
    ShowEditEmployeeDialog: function () {
        var self = this;
        var selectedId = $('input[name=rdSelected]:checked').val();
        if (selectedId) {
            var app = { employee: self.GetEmployeeById(selectedId), departments: Departments };
            var myTemplate = $.templates("#tmplCreateEditEmployee");
            var html = myTemplate.render(app);
            $(html).dialog({
                title: "Edit Employee",
                buttons: [
                    {
                        text: "Edit",
                        click: function () {
                            self.EditEmployee($('#hdnId').val(), $('#txtName').val(), $('#selDepartment').val());
                            $(this).dialog("destroy").remove();
                        }
                    },
                    {
                        text: "Cancel",
                        click: function () {
                            $(this).dialog("destroy").remove();
                        }
                    }]
            });
        }
        else {
            self.ShowMessageDialog("Info", "Please select an employee!");
        }
    },
    //********************************************************
    //Method for retrieving an employee by id
    //********************************************************
    GetEmployeeById: function (id) {
        var self = this;
        if (id && self.Employees) {
            for (var i = 0; i < self.Employees.length; i++) {
                if (self.Employees[i].Id == id) {
                    return self.Employees[i];
                }
            }
        }
    }
};
//********************************************************
//Object that holds departments info
//********************************************************
var Departments = [
    { name: "Window Cleaning" },
    { name: "Hoovering" },
    { name: "Polishing" }
];
//*******************************************************************
//Defining handlers for ajax start and stop that block/unblock the UI
//*******************************************************************
$(document).on({
    ajaxStart: function () {
        $.blockUI(BlockUIOptions);
    },
    ajaxStop: function () {
        $.unblockUI();
    }
});