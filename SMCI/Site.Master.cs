﻿using System;
using System.Web.Security;

namespace SMCI
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        public bool IsAuthenticated = false;

        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            IsAuthenticated = Page.User.Identity.IsAuthenticated;
            if (IsAuthenticated)
            {
                LoginCtrl.Visible = false;
            }

        }

        /// <summary>
        /// Login control authentcate event handler.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void LoginCtrl_Authenticate(object sender, System.Web.UI.WebControls.AuthenticateEventArgs e)
        {
            if (FormsAuthentication.Authenticate(LoginCtrl.UserName, LoginCtrl.Password))
            {
                e.Authenticated = true;
            }
        }
    }
}
