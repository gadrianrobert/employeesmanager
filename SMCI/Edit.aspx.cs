﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SMCI
{
    public partial class About : System.Web.UI.Page
    {
        protected int employeeId;

        private const string IdQueryStringParam = "Id";

        /// <summary>
        /// Page load event handler.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            int.TryParse(Request.QueryString[IdQueryStringParam], out employeeId);
        }
    }
}
